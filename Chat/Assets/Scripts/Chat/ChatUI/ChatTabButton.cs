﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatTabButton : MonoBehaviour
{
    private ChatChanel chatChanel;
    public ChatChanel ChatChanel
    {
        get
        {
            return chatChanel;
        }
        set
        {
            chatChanel = value;
            SetChatName(chatChanel.ChatName.ToString());
        }
            
    }

    public delegate void ChatTabButtonClkDel(ChatChanel chatChanel);
    public static event ChatTabButtonClkDel ChatTabButtonClkEvent;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(BtnClk);
        BtnClk();
    }

    private void SetChatName(string chatName)
    {
        GetComponentInChildren<Text>().text = chatName;
    }

    private void BtnClk()
    {
        if (ChatTabButtonClkEvent != null)
        {
            ChatTabButtonClkEvent(chatChanel);
        }
    }    
}
