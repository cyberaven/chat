﻿using UnityEngine;
using UnityEditor;

public class Panel : MonoBehaviour
{
    float currentTime = 0;
    float showTime = 0;

    private void Update()
    {
        ShowTime();
    }
    public void Show()
    {
        Debug.Log("Panel " + this.gameObject.name + " show.", gameObject);
        gameObject.SetActive(true);
    }
    public void Show(float time)
    {
        Show();
        Debug.Log("Panel " + this.gameObject.name + " show. Time: " + time, gameObject);
        showTime = time;
    }
    public void Hide()
    {
        Debug.Log("Panel " + this.gameObject.name + " hide.", gameObject);
        gameObject.SetActive(false);
    }
    void ShowTime()
    {
        if (showTime > 0)
        {
            currentTime += Time.deltaTime;
            if (currentTime > showTime)
            {
                showTime = 0;
                currentTime = 0;
                Hide();
            }
        }
    }
}