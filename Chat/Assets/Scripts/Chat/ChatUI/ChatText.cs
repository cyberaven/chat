﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatText : MonoBehaviour
{
    Text t;   
    private void Awake()
    {
        t = GetComponent<Text>();       
    }
    public void SetText(string msg, Color color)
    {        
        t.text = msg;
        t.color = color;
    }
}
