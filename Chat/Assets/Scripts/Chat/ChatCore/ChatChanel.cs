﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChatName
{
    Main,
    Party,
    Clan,
    System
}

public class ChatChanel : MonoBehaviour
{
    [SerializeField] ChatName chatName;
    public ChatName ChatName { get => chatName; set => chatName = value; }    

    [SerializeField] Color baseTextColor = Color.black;
    public Color BaseTextColor { get => baseTextColor; set => baseTextColor = value; }

    List<MessageData> messages = new List<MessageData>();
    public List<MessageData> Messages { get => messages; set => messages = value; }

    int maxCountMsg = 100;   

    public delegate void ChatChanelCreateDel(ChatChanel chatChanel);
    public static event ChatChanelCreateDel ChatCreateEvent;

    public delegate void ChatChanelSendMessageDel(ChatChanel chatChanel, string msg);
    public static event ChatChanelSendMessageDel ChatChanelSendMessageEvent;

    public delegate void NewMessageInDel(ChatChanel chatChanel);
    public static event NewMessageInDel NewMessageInEvent;

    private void Start()
    {
        ChatCreate();        
    }

    private void ChatCreate()
    {        
        //TO DO: Регистрация чата.
        if (ChatCreateEvent != null)
        {
            ChatCreateEvent(this);
        }
    }
   
    public void MessageIn(string msg)
    {
        CheckMsgListSize();

        Messages.Add(new MessageData(msg, baseTextColor));

        if(NewMessageInEvent != null)
        {
            NewMessageInEvent(this);
        }
    }
    public void MessageIn(string msg, Color color)
    {
        CheckMsgListSize();

        Messages.Add(new MessageData(msg, color));

        if (NewMessageInEvent != null)
        {
            NewMessageInEvent(this);
        }
    }

    public void MessageOut(string msg)
    {
        //TO DO: Для тестирования сделано кольцо, удалить при внедрении
        ChatCore.INCTANCE.NewChatMessageIn(this.ChatName, msg);
        //TO DO: Для тестирования сделано кольцо, удалить при внедрении

        if (ChatChanelSendMessageEvent != null)
        {
            ChatChanelSendMessageEvent(this, msg);
        }        
    }
    private void CheckMsgListSize()
    {       
        if (messages.Count == maxCountMsg)
        {            
            RepackMsgList();           
        }
    }
    private void RepackMsgList()
    {
        List<MessageData> newMsgs = new List<MessageData>();
        for (int i = 0; i < messages.Count; i++)
        {
            if (i < messages.Count -1)
            {
                newMsgs.Add(messages[i + 1]);
            }
        }
        messages = null;
        messages = newMsgs;
    }
}
