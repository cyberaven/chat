﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatTabPanel : MonoBehaviour
{
    [SerializeField] ChatTabButton chatTabButton;

    private void Start()
    {
        ChatChanel.ChatCreateEvent += CreateChatTab;
    }
    private void OnDestroy()
    {
        ChatChanel.ChatCreateEvent -= CreateChatTab;
    }
    private void CreateChatTab(ChatChanel chatChanel)
    {
        if (chatChanel.ChatName != ChatName.System)
        {
            ChatTabButton chtBtn = Instantiate(chatTabButton, transform);
            chtBtn.ChatChanel = chatChanel;            
        }
    }
}
