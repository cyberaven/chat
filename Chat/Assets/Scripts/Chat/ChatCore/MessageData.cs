﻿using UnityEngine;

public class MessageData
{
    string txt;
    public string Txt { get => txt; set => txt = value; }

    Color color;
    public Color Color { get => color; set => color = value; }    

    public MessageData(string t, Color c)
    {
        txt = t;
        color = c;
    }
}
