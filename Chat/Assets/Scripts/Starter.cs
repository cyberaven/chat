﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Starter : MonoBehaviour
{
    [SerializeField] GameUI gameUI;
    [SerializeField] ChatCore chatCore;

    private void Awake()
    {
        gameUI = Instantiate(gameUI);
        chatCore = Instantiate(chatCore);
    }
}
