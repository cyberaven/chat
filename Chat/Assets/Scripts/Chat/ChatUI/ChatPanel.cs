﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatPanel : Panel
{
    [SerializeField] ChatTextView chatTextView;
    [SerializeField] ChatTextView systemChatTextView;

    private void Start()
    {
        ChatChanel.ChatCreateEvent += CreateChatTextView;
    }
    private void OnDestroy()
    {
        ChatChanel.ChatCreateEvent -= CreateChatTextView;
    }

    private void CreateChatTextView(ChatChanel chatChanel)
    {
        ChatTextView textView;

        if (chatChanel.ChatName != ChatName.System)
        {
            textView = Instantiate(chatTextView, transform);            
        }
        else
        {
            textView = Instantiate(systemChatTextView, transform);
        }
        textView.ChatChanel = chatChanel;
    }
}
