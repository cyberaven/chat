﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatCore : MonoBehaviour
{
    public static ChatCore INCTANCE;

    [Header("Базовые чаты")]
    [SerializeField] List<ChatChanel> baseChats = new List<ChatChanel>();   

    [Header("Текущие чаты")]
    [SerializeField] List<ChatChanel> currentChats = new List<ChatChanel>();

    [SerializeField] ChatChanel activeChatChanel;
    
    #region Unity method
    private void Awake()
    {
        // TO DO: Переделать на нормальный синглтон. 
        INCTANCE = this;//Ленивый синглтог

        ChatChanel.ChatCreateEvent += NewChatChanelCreated;
        ChatInputField.UserInputNewMessageEvent += UserInputNewMessage;
        ChatTabButton.ChatTabButtonClkEvent += ChatTabButtonClk;
        LoadChanel();
    }
    private void Start()
    {
        ShowChatUI();
    }
    private void OnDestroy()
    {
        SaveChanel();
        ChatChanel.ChatCreateEvent -= NewChatChanelCreated;
        ChatInputField.UserInputNewMessageEvent -= UserInputNewMessage;
        ChatTabButton.ChatTabButtonClkEvent -= ChatTabButtonClk;
    }
    #endregion


    public void NewChatMessageIn(ChatName chatName, string msg)
    {
        foreach (ChatChanel chatChanel in currentChats)
        {
            if(chatChanel.ChatName == chatName)
            {
                chatChanel.MessageIn(msg);
            }
        }
    }
    public void NewChatMessageIn(ChatName chatName, string msg, Color color)
    {
        foreach (ChatChanel chatChanel in currentChats)
        {
            if (chatChanel.ChatName == chatName)
            {
                chatChanel.MessageIn(msg, color);
            }
        }
    }

    private void ShowChatUI()
    {
        GameUI.INCTANCE.ChatPanel.Show();
    }

    //Знаю что чаты будут фиксированные, но чтот захотелось расширения.
    private void LoadChanel()
    {
        // TODO: Тут должно быть условие, если сохраненых чатов нет выводим базовые.
        LoadBaseChanel();
    }
    private void SaveChanel()
    {
        // TODO: Сохраняем чаты которые в данный момент были открыты. При закрытии приложения использовать.
    }

    private void LoadBaseChanel()
    {
        foreach (ChatChanel chatChanel in baseChats)
        {
            Instantiate(chatChanel, transform);
        }
    }
    private void NewChatChanelCreated(ChatChanel chatChanel)
    {
        currentChats.Add(chatChanel);
    }
    private void UserInputNewMessage(string msg)
    {
        activeChatChanel.MessageOut(msg);
    }
    private void ChatTabButtonClk(ChatChanel chatChanel)
    {
        activeChatChanel = chatChanel;
    }


}
