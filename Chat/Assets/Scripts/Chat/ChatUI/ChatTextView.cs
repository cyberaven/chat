﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatTextView : MonoBehaviour
{
    [SerializeField] ChatChanel chatChanel;
    [SerializeField] GameObject content;
    [SerializeField] ChatText chatText;
    [SerializeField] Scrollbar vertScrolbar;   

    public ChatChanel ChatChanel
    {
        get
        {
            return chatChanel;
        }
        set
        {
            chatChanel = value;            
        }
    }

    private void Start()
    {
        ChatTabButton.ChatTabButtonClkEvent += OnOffTextView;
        ChatChanel.NewMessageInEvent += NewMessageIn;
    }
    private void OnDestroy()
    {
        ChatTabButton.ChatTabButtonClkEvent -= OnOffTextView;
        ChatChanel.NewMessageInEvent -= NewMessageIn;
    }
    private void OnEnable()
    {
        if (chatChanel != null)
        {
            ViewChatChanelMsg();
        }
    }

    private void OnOffTextView(ChatChanel chatChanel)
    {
        if (this.chatChanel.ChatName != ChatName.System)//систем никогда не выкл
        {
            if (this.chatChanel.ChatName == chatChanel.ChatName)
            {
                gameObject.SetActive(true);                             
            }
            else
            {              
                gameObject.SetActive(false);
            }            
        }
    } 
    private void ViewChatChanelMsg()
    {
        DelAllTChatext();
        foreach (MessageData md in chatChanel.Messages)
        {
            CreateChatText(md.Txt, md.Color);
        }
        StartCoroutine(ScrolbarMoveDown());
    }
    private void DelAllTChatext()
    {
        ChatText[] chatTexts = content.GetComponentsInChildren<ChatText>();
        foreach (ChatText ct in chatTexts)
        {
            Destroy(ct.gameObject);
        }
    }

    private void NewMessageIn(ChatChanel chatChanel)
    {
        if (this.chatChanel.ChatName == chatChanel.ChatName)
        {
            ViewChatChanelMsg();
        }  
    } 
    private void CreateChatText(string msg, Color color)
    {        
        ChatText ct = Instantiate(chatText, transform);
        ct.transform.SetParent(content.transform);
        ct.SetText(msg, color);
    }
    private IEnumerator ScrolbarMoveDown()
    {
        yield return new WaitForSeconds(0.2f);        
        vertScrolbar.value = 0f;        
    }
}
