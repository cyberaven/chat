﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatInputField : MonoBehaviour
{
    [SerializeField] InputField inputField;

    public delegate void UserInputNewMessageDel(string message);
    public static event UserInputNewMessageDel UserInputNewMessageEvent;
    private void Update()
    {
        if (Input.GetKeyDown("enter") || Input.GetKeyDown("return"))
        {
            if (inputField.text != "")
            {                 
                UserInputNewMessage(inputField.text);
                inputField.text = "";
            }
        }
    }
    private void UserInputNewMessage(string newMessage)
    {
        if(UserInputNewMessageEvent != null)
        {
            UserInputNewMessageEvent(newMessage);
        }
    }
}
