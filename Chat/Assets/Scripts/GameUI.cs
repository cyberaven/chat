﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public static GameUI INCTANCE;


    [SerializeField] ChatPanel chatPanel;
    public ChatPanel ChatPanel { get => chatPanel; set => chatPanel = value; }


    private void Awake()
    {
        // TO DO: Переделать на нормальный синглтон. 
        INCTANCE = this;//Ленивый синглтог

        chatPanel = Instantiate(chatPanel, transform);
    }
    private void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;

    }
}
